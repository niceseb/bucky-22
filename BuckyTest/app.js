var fs = require('fs');

fs.writeFileSync("corn.txt", "This is auto-generated - do not edit. Corn is good, corn is life");

console.log(fs.readFileSync("corn.txt").toString());


var path = require('path');
var websiteHome = "Desktop/Bucky//thenewboston/index.html";
var websiteAbout = "Desktop/Bucky/thenewboston/about.html";

console.log(path.normalize(websiteHome));
console.log(path.dirname(websiteAbout));
console.log(path.basename(websiteAbout));
console.log(path.extname(websiteAbout));

//ans
//Desktop/Bucky/thenewboston/index.html - automatically corrects for double //
//Desktop/Bucky/thenewboston - just the directory
//about.html - just the file name
//.html - just the extension

//to call every 2 seconds
//setInterval(function () {
//    console.log("beef");
//}, 2000);

console.log(__dirname);
console.log(__filename);