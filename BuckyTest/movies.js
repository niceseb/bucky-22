//This is shared object state so anytime a module makes change, it will also
//other modules that imported it
//module.exports = {
//    favMovie: ""
//}

//To make it an Object factory, so each time module gets its own copy of favMovie

module.exports = function () {
    return {
        favMovie: ""
    }
};